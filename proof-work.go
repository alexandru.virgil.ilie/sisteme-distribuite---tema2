package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/bits"
	//"time"
	//"strconv"
)


func makeHash( record string ) []byte {
	fmt.Println("In makeHash: ")
	fmt.Println("String: " + record)
	h := sha256.New()
	h.Write([]byte(record))
	
	hashed := h.Sum(nil)
	fmt.Printf("%b", hashed)  // 1101101
	fmt.Println()
	fmt.Println("Hex: " + hex.EncodeToString(hashed))
	fmt.Println()
	return hashed
	//return hex.EncodeToString(hashed)
}

func checkZeroes( hashed []byte ) bool {
	var flag bool = true


		//1
		for i := 0; i < 2; i++{
			if (bits.OnesCount(uint(hashed[i])) != 0) {
				flag = false
				break
			}
		}
		
		
		fmt.Printf( "%08b", hashed[0] )
		fmt.Println()
		
		//2
		/*for i := 0; i < 7; i++{
			if uint(hashed[0])&(1<<uint(i)) != 0 {
				flag = false
				break
			}
		}*/
		
		fmt.Println( flag )
		return flag
}

func main(){
	testString := "Hello World" // 
	record := makeHash(testString)
	
	var i int = 0
	for checkZeroes( record ) == false {
		testString = fmt.Sprintf("%s%d", testString, i)
		record = makeHash(testString)
		i++
		testString = "Hello World"
		//time.Sleep(100 * time.Millisecond)
	}
	
}










//how many zeroes
	/*for i := 0; i < len(string(hashed[0])); i++ {
		if hashed[i]&(1<<uint8(i)) != 0 {
			
			fmt.Println("Got one!")
		}
	}*/
	
	//fmt.Println( bits.OnesCount( uint(hashed[0]) ) )
	
	//fmt.Println()
	
	
	
	//fmt.Printf("%08b", hashed[0])  // 1101101
	//fmt.Println()