# Sisteme Distribuite - Tema2

Aplicatie care simuleaza functionarea unui Blockchain centralizat + Power of work

<p>Comenzi Docker:</p> 
docker network create golang-network

<p><h4>Primul container:</h4></p>
<p>docker run -dit --name server --network=golang-network -v LOCATIE_FISIERE:/go/src/app golang-image</p>
<p>go get github.com/davecgh/go-spew/spew | go get github.com/gorilla/mux | go get github.com/joho/godotenv</p>

<p><h4>Al doilea container:</h4></p>
<p>docker run -dit --name host--network=golang-network -v LOCATIE_FISIERE:/go/src/app golang-image</p>
<p>curl -d '{"BPM": 50}' -H "Content-Type: application/json" -X POST 172.18.0.2:8080</p>
<p>'pentru aflare IP-ul primului container' -> docker network inspect golang-network</p>